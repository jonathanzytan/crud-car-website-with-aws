import {Component, Input, OnInit} from '@angular/core';
import {Car} from "../car";
import {CarService} from "../car.service";
import {ActivatedRoute} from "@angular/router";
//import {Location} from "@angular/common";
import {Router} from "@angular/router";

@Component({
  selector: 'app-car-edit',
  templateUrl: './car-edit.component.html',
  styleUrls: ['./car-edit.component.css']
})
export class CarEditComponent implements OnInit {

  @Input()
  car: Car;

  constructor(private carService: CarService,
              private route: ActivatedRoute,
              //private location: Location,
              private router: Router) { }

  ngOnInit() { this.getCar(); }

  getCar(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.carService.getCar(id).subscribe(car => this.car = car);
  }

  goBack(): void {
    this.router.navigateByUrl('/viewcar');
  }

  save(): void {
    this.carService.updateCar(this.car.id, this.car).subscribe(() => this.goBack());
    this.goBack();
    this.ngOnInit();
  }

}
