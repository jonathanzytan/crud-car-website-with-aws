import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CarDetailComponent } from './car-detail/car-detail.component';
import { CarViewComponent } from './car-view/car-view.component';
import { CarCreateComponent } from './car-create/car-create.component';
import { CarSearchComponent } from './car-search/car-search.component';
import { HttpClientModule } from "@angular/common/http";
import {CarService} from "./car.service";
import {NgxPaginationModule} from "ngx-pagination";
import { CarEditComponent } from './car-edit/car-edit.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    CarDetailComponent,
    CarViewComponent,
    CarCreateComponent,
    CarSearchComponent,
    CarEditComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
