package car;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
    public class CarController {

     @Autowired
     private CarRepository carRepository;

    @Autowired
    AmazonS3ClientService AmazonS3ClientService;

    @Value("${filepath}")
    private String filePath;

     @EventListener
     public void appReady(ApplicationReadyEvent event) {
         downloadFile();
         populateRepo();
//         String carModel;
//         String carBrand;
//         int carYear;
//         int carPrice;
//         String[] carBrands = new String[]{"Toyota", "Honda", "Mitsubishi", "BMW", "Ford", "Holden", "Mazda", "Volkswagen", "Nissan", "Audi"};
//         String[] carModels = new String[]{"Alpha", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot", "Golf", "Hotel", "Lima", "Mike", "November"};
//
//         for (int i = 0; i <= 1005; i++) {
//             carYear = (int) (Math.random() * 120 + 1900);
//             carPrice = (int) (Math.random() * 1000000);
//             carBrand = carBrands[(int) (Math.random() * carBrands.length)];
//             carModel = carModels[(int) (Math.random() * carModels.length)];
//             Car car = new Car();
//             car.setCarBrand(carBrand);
//             car.setCarModel(carModel);
//             car.setCarYear(carYear);
//             car.setCarPrice(carPrice);
//             carRepository.save(car);
//         }
     }

    @PostMapping("/car/create")
    public Car createCar(@RequestBody Car car) throws IOException{
        Car car1 = carRepository.save(car);
        downloadRepo();
        deleteFile();
        uploadFile();
        return car1;
    }

    @GetMapping("/car/view")
    public List<Car> viewAllCars(@RequestParam(name = "sortOrder", required = false, defaultValue = "") String sortOrder ) {
         if (sortOrder.equalsIgnoreCase("ascbrand")) {
            Sort sort = new Sort(Sort.Direction.ASC, "carBrand", "carModel").and(new Sort(Sort.Direction.DESC, "carYear", "carPrice").and(new Sort(Sort.Direction.ASC, "id")));
            return carRepository.findAll(sort);
        }
        else if (sortOrder.equalsIgnoreCase("descbrand")){
            Sort sort = new Sort(Sort.Direction.DESC, "carBrand").and(new Sort(Sort.Direction.ASC, "carModel")).and(new Sort(Sort.Direction.DESC, "carYear", "carPrice").and(new Sort(Sort.Direction.ASC, "id")));
            return carRepository.findAll(sort);
        }
        else if (sortOrder.equalsIgnoreCase("ascmodel")){
            Sort sort = new Sort(Sort.Direction.ASC, "carModel", "carBrand").and(new Sort(Sort.Direction.DESC, "carYear", "carPrice").and(new Sort(Sort.Direction.ASC, "id")));
            return carRepository.findAll(sort);
        }
        else if (sortOrder.equalsIgnoreCase("descmodel")){
            Sort sort = new Sort(Sort.Direction.DESC, "carModel").and(new Sort(Sort.Direction.ASC, "carBrand")).and(new Sort(Sort.Direction.DESC, "carYear", "carPrice").and(new Sort(Sort.Direction.ASC, "id")));
            return carRepository.findAll(sort);
        }
        else if (sortOrder.equalsIgnoreCase("ascyear")){
            Sort sort = new Sort(Sort.Direction.ASC, "carYear").and(new Sort(Sort.Direction.ASC, "carBrand", "carModel").and(new Sort(Sort.Direction.ASC, "carPrice")).and(new Sort(Sort.Direction.ASC, "id")));
            return carRepository.findAll(sort);
        }
        else if (sortOrder.equalsIgnoreCase("descyear")){
            Sort sort = new Sort(Sort.Direction.DESC, "carYear").and(new Sort(Sort.Direction.ASC, "carBrand", "carModel").and(new Sort(Sort.Direction.ASC, "carPrice")).and(new Sort(Sort.Direction.ASC, "id")));
            return carRepository.findAll(sort);
        }
        else if (sortOrder.equalsIgnoreCase("ascprice")){
            Sort sort = new Sort(Sort.Direction.ASC, "carPrice").and(new Sort(Sort.Direction.ASC, "carBrand", "carModel").and(new Sort(Sort.Direction.ASC, "carYear")).and(new Sort(Sort.Direction.ASC, "id")));
            return carRepository.findAll(sort);
        }
        else if (sortOrder.equalsIgnoreCase("descprice")){
            Sort sort = new Sort(Sort.Direction.DESC, "carPrice").and(new Sort(Sort.Direction.ASC, "carBrand", "carModel").and(new Sort(Sort.Direction.ASC, "carYear")).and(new Sort(Sort.Direction.ASC, "id")));
            return carRepository.findAll(sort);
        }
        else {return carRepository.findAll();}
    }

    @GetMapping(value = "/car/view/{id}")
    public Car getCar(@PathVariable int id) {
        Car car = carRepository.findById(id).get();
        return car;
    }

    @DeleteMapping("/car/view/delete/{id}") /*Delete a car*/
    public ResponseEntity<String> deleteCar(@PathVariable("id") int id, Model model) throws IOException{
        Car car = carRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid User Id: " + id));
        carRepository.delete(car);
        downloadRepo();
        deleteFile();
        uploadFile();
        return new ResponseEntity<>("Car Deleted", HttpStatus.OK);
    }

    @PutMapping("/car/view/edit/{id}")
    public ResponseEntity<String> updateCar(@PathVariable("id") int id, @RequestBody Car car) throws IOException{
        Optional<Car> carData = carRepository.findById(id);
        Car car1 = carData.get();
        car1.setCarModel(car.getCarModel());
        car1.setCarBrand(car.getCarBrand());
        car1.setCarYear(car.getCarYear());
        car1.setCarPrice(car.getCarPrice());
        carRepository.save(car1);
        downloadRepo();
        deleteFile();
        uploadFile();
        return new ResponseEntity<>("Car Updated", HttpStatus.OK);
    }

    @GetMapping("/car/view/search")
    public List<Car> searchCar(@RequestParam(name = "car") String term) {
        List<Car> cars = new ArrayList<>();
        //List<Car> correctCars = new ArrayList<>();
        String carBrand = term;
        String carModel = term;
        carRepository.findByCarBrandContainsOrCarModelContains(carBrand,carModel).forEach(cars::add);
//        carRepository.findAll(term).forEach(cars::add);
//        for (Car car : cars){
//            if (car.getCarBrand().toLowerCase().contains(term.toLowerCase())) {
//                correctCars.add(car);
//            }
//            else if (car.getCarModel().toLowerCase().contains(term.toLowerCase())) {
//                correctCars.add(car);
//            }
//        }
//        return correctCars;
        return cars;
    }

//    @PostMapping("/uploadPostman")
//    public String uploadFilePostman(@RequestParam("file") MultipartFile file) throws IOException {
//        String keyName = file.getOriginalFilename();
//        AmazonS3ClientService.uploadFile(keyName, file.getInputStream());
//        return "Upload Successfully -> KeyName = " + keyName;
//    }

    @GetMapping("/upload")
    public String uploadFile() throws IOException {
        String keyName = "cars.json";
        AmazonS3ClientService.uploadFile(keyName, new FileInputStream(filePath));
        return "Upload Successfully -> KeyName = " + keyName;
    }


    @GetMapping("/delete")
    public String deleteFile() {
        String fileName = "cars.json";
        AmazonS3ClientService.deleteFile(fileName);
        return "Delete Successful -> FileName = " + fileName;
    }

    /*
     * Download Files
     */
    @GetMapping("/download")
    public String downloadFile() {
        String keyname = "cars.json";
        ByteArrayOutputStream downloadInputStream = AmazonS3ClientService.downloadFile(keyname);

        return "Download Successful -> FileName = " + keyname;
    }

    public void populateRepo(){
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<Car>> mapType = new TypeReference<List<Car>>() {};
        try {
            List<Car> carList = mapper.readValue(new FileInputStream(filePath), mapType);
            carRepository.saveAll(carList);
            System.out.println("Car list populated successfully");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void downloadRepo(){
        ObjectMapper mapper = new ObjectMapper();
        List<Car> car = carRepository.findAll();
        try {
            // Convert object to JSON string and save into a file directly
            mapper.writeValue(new File(filePath), car);
            System.out.println("Car list saved successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * List ALL Files
     */
//    @GetMapping("/allfiles")
//    public List<String> listAllFiles(){
//        return AmazonS3ClientService.listFiles();
//    }

    private MediaType contentType(String keyname) {
        String[] arr = keyname.split("\\.");
        String type = arr[arr.length-1];
        switch(type) {
            case "txt": return MediaType.TEXT_PLAIN;
            case "png": return MediaType.IMAGE_PNG;
            case "jpg": return MediaType.IMAGE_JPEG;
            default: return MediaType.APPLICATION_OCTET_STREAM;
        }
    }
}