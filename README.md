Just a CRUD Car Website (AWS S3)
===

This project was done using Angular6, Spring Boot and AWS S3 Bucket.
This is a simple project done to create a website with CRUD capabilities that utilizes both Angular6 as well as AWS S3.
Spring Security is also used for adding cars. (You have to be ""logged in" to be able to add cars)

Build
---

`./gradlew build`

Run
---
Either do
`./gradlew bootRun | ./gradlew ngServe` on the same command line

OR

`./gradlew bootRun`
`./gradlew ngServe`
on seperate command lines

Adding Cars
---
To add cars, you require to be logged in. The login details are as follows:
user: `user`
pw: `abc1234`