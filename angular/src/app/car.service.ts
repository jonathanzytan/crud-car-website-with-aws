import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Car} from './car';
import {viewAttached} from '@angular/core/src/render3/instructions';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  private baseUrl = 'http://localhost:8080/car';

  constructor(private http: HttpClient) { }

  getCar(id: number): Observable<Car> {
    return this.http.get<Car>(`${this.baseUrl}` + `/view/${id}`);
  }

  createCar(car: Car): Observable<Car> {
    const myItem = sessionStorage.getItem('token');
    const headers = new HttpHeaders({ Authorization: 'Basic ' + myItem });
    return this.http.post<Car>(`${this.baseUrl}` + `/create`, car, {headers});
  }

  updateCar(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/view/edit/${id}`, value);
  }

  deleteCar(id: number): Observable<Object> {
    return this.http.delete(`${this.baseUrl}/view/delete/${id}`);
  }

  getAllCars(sortOrder: string): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `/view?sortOrder=${sortOrder}`);
  }

  searchCars(term: string): Observable<Car[]> {
    if (!term.trim()) {
      return of(([]));
    }
    return this.http.get<Car[]>(`${this.baseUrl}/view/search?car=${term}`);

  }

}
