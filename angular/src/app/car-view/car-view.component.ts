import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {CarService} from "../car.service";
import {Car} from "../car";

@Component({
  selector: 'car-view',
  templateUrl: './car-view.component.html',
  styleUrls: ['./car-view.component.css']
})
export class CarViewComponent implements OnInit {

  cars: Observable<Car[]>;
  order: string = '';
  p: number = 1;
  @Input()
  car: Car;


  constructor(private carService: CarService) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
  }

  orderByBrand() {
    if (this.order ==  'ascbrand'){
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'descbrand';
    }
    else if (this.order ==  'descbrand') {
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'ascbrand';
    }
    else {
      this.order = 'ascbrand';
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'descbrand';
    }
  }

  orderByModel() {
    if (this.order ==  'ascmodel'){
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'descmodel';
    }
    else if (this.order ==  'descmodel') {
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'ascmodel';
    }
    else {
      this.order = 'ascmodel';
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'descmodel';
    }
  }

  orderByYear() {
    if (this.order ==  'ascyear'){
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'descyear';
    }
    else if (this.order ==  'descyear') {
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'ascyear';
    }
    else {
      this.order = 'ascyear';
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'descyear';
    }
  }

  orderByPrice() {
    if (this.order ==  'ascprice'){
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'descprice';
    }
    else if (this.order ==  'descprice') {
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'ascprice';
    }
    else {
      this.order = 'ascprice';
      this.carService.getAllCars(this.order).subscribe(cars => this.cars = cars);
      this.order = 'descprice';
    }
  }

}
