package car;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

public interface AmazonS3ClientService
{
    public ByteArrayOutputStream downloadFile(String keyName);
    public void uploadFile(String keyName, InputStream file);
    public List<String> listFiles();
    public void deleteFile(String fileName);
}