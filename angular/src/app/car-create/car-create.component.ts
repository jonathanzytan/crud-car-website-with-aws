import { Component, OnInit } from '@angular/core';
import {CarService} from "../car.service";
import {Car} from "../car";

@Component({
  selector: 'car-create',
  templateUrl: './car-create.component.html',
  styleUrls: ['./car-create.component.css']
})
export class CarCreateComponent implements OnInit {

  car: Car = new Car();
  submitted = false;

  constructor(private carService: CarService) { }

  ngOnInit() {
  }

  newCar(): void {
    this.submitted = false;
    this.car = new Car();
  }

  save() {
    this.carService.createCar(this.car).subscribe(data => console.log(data), error => console.log(error));
    this.car = new Car();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

}
