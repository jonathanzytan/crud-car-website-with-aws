package car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Serializable> {
    List<Car> findByCarBrandContainsOrCarModelContains(String carBrand, String carModel);
}