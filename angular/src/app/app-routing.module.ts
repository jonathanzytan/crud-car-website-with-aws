import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CarViewComponent} from "./car-view/car-view.component";
import {CarCreateComponent} from "./car-create/car-create.component";
import {CarSearchComponent} from "./car-search/car-search.component";
import {CarDetailComponent} from "./car-detail/car-detail.component";
import {CarEditComponent} from "./car-edit/car-edit.component";
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  {path: '', redirectTo: 'car', pathMatch: 'full'},
  {path: 'viewcar', component: CarViewComponent},
  {path: 'createcar', component: CarCreateComponent},
  {path: 'detail/:id', component: CarDetailComponent},
  {path: 'editcar/:id', component: CarEditComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
