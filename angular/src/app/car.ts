export class Car {
  id: number;
  carBrand: string;
  carModel: string;
  carYear: number;
  carPrice: number;
}
