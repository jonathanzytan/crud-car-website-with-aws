import {Component, Input, OnInit} from '@angular/core';
import {CarService} from "../car.service";
import {Car} from "../car";
import {Router} from "@angular/router";
import {ActivatedRoute} from '@angular/router';
//import {Location} from '@angular/common';

@Component({
  selector: 'car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.css']
})
export class CarDetailComponent implements OnInit {

  @Input()
  car: Car;

  constructor(private carService: CarService,
              private route: ActivatedRoute,
              //private location: Location,
              private router: Router) { }

  ngOnInit() { this.getCar(); }

  getCar(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.carService.getCar(id).subscribe(car => this.car = car);
  }

  goBack(): void {
    this.router.navigateByUrl('/viewcar');
  }

  deleteCar(car: Car): void {
    this.carService.deleteCar(this.car.id).subscribe();
    this.goBack();
    this.ngOnInit();
  }

}
